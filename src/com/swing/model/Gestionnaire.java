package com.swing.model;

/**
 * Utilisateur dont le but est de gérer l'application.
 */
public class Gestionnaire extends Profil {

  /**
   * Crée un gestionnaire selon les paramètres donnés.
   * 
   * @param pseudo Le pseudo du gestionnaire.
   * @param motDePasse Le mot de passe du gestionnaire.
   */
  public Gestionnaire(String pseudo, String motDePasse) {
    super(pseudo, motDePasse);
  }

}
