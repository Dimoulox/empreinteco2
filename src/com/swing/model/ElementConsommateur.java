package com.swing.model;

import com.swing.db.persistance.Persistable;

/**
 * Elément consommant du CO2.
 */
public class ElementConsommateur extends Persistable<Integer> {


  /** Taille minimale du nom. */
  private static int TAILLE_MIN_NOM = 3;

  /** Taille maximale du nom. */
  private static int TAILLE_MAX_NOM = 45;

  /** Nom de l'élément consommateur. */
  private String nom;

  /** Catégorie de l'élément consommateur. */
  private Categorie categorie;

  /**
   * Kg de CO2 émis par unité de consommation.
   */
  private Taux taux;

  /**
   * Unités de consommations par unité d'utilisation.
   */
  private double consommation;

  /**
   * Nombre de personnes partageant l'élément.
   */
  private int partage;

  /**
   * Crée un élément consommateur avec les paramètres donnés.
   * 
   * @param nom Le nom de l'élément.
   * @param categorie La catégorie de l'élément.
   * @param taux Les kg de CO2 émis par l'élément par unité de consommation.
   * @param consommation Le nombre d'unités de consommation de l'élément par unité d'utilisation.
   * @param partage Le nombre de personnes partageant l'élément.
   */
  public ElementConsommateur(String nom, Categorie categorie, Taux taux, double consommation,
      int partage) {
    if (nom == null) {
      throw new IllegalArgumentException("L'argument nom doit ne pas être null");
    }
    if (nom.length() < TAILLE_MIN_NOM) {
      throw new IllegalArgumentException(
          "L'argument nom doit être de plus de " + TAILLE_MIN_NOM + " caractères");
    }
    if (nom.length() >= TAILLE_MAX_NOM) {
      throw new IllegalArgumentException(
          "L'argument nom doit être de moins de " + TAILLE_MAX_NOM + " caractères");
    }
    if (partage <= 0) {
      throw new IllegalArgumentException("L'argument partage doit être supérieur à 0");
    }
    if (taux == null) {
      throw new IllegalArgumentException("L'argument taux doit ne pas être null");
    }
    if (consommation <= 0.) {
      throw new IllegalArgumentException("L'argument consommation doit être supérieur à 0");
    }
    if (categorie == null) {
      throw new IllegalArgumentException("L'argument categorie doit ne pas être null");
    }
    this.nom = nom;
    this.categorie = categorie;
    this.taux = taux;
    this.consommation = consommation;
    this.partage = partage;
  }

  /**
   * Calcule et retourne la consommation de l'élément selon une utilisation donnée.
   * 
   * @param utilisation L'utilisation de l'élément.
   */
  public double calculerConsommation(double utilisation) {
    if (utilisation < 0.) {
      throw new IllegalArgumentException("L'argument utilisation doit être supérieur ou égal à 0");
    }
    return taux.getValeur() * consommation * utilisation / partage;
  }

  /**
   * Retourne la catégorie de l'élément.
   */
  public Categorie getCategorie() {
    return this.categorie;
  }

  /**
   * Retourne le nom de l'élément.
   */
  public String getNom() {
    return this.nom;
  }

}
