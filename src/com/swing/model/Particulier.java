package com.swing.model;

import java.util.ArrayList;

/**
 * Utilisateur dont le but est de calculer son empreinte CO2.
 */
public class Particulier extends Profil {

  /** Consommations mensuelles du particulier. */
  private ArrayList<ConsommationMensuelle> consommationsMensuelles;

  /**
   * Crée un particulier avec des consommations mensuelles.
   * 
   * @param consommationsMensuelles Les consommations mensuelles du particulier.
   * @param pseudo Le pseudo du particulier.
   * @param motDePasse Le mot de passe du particulier.
   */
  public Particulier(ArrayList<ConsommationMensuelle> consommationsMensuelles, String pseudo,
      String motDePasse) {
    super(pseudo, motDePasse);
    if (consommationsMensuelles == null) {
      throw new IllegalArgumentException(
          "L'argument consommationsMensuelles doit ne pas être null");
    }
    this.consommationsMensuelles = consommationsMensuelles;
  }

  /**
   * Crée un particulier sans consommations mensuelles.
   * 
   * @param pseudo Le pseudo du particulier.
   * @param motDePasse Le mot de passe du particulier.
   */
  public Particulier(String pseudo, String motDePasse) {
    super(pseudo, motDePasse);
    this.consommationsMensuelles = new ArrayList<ConsommationMensuelle>();
  }

  /**
   * Ajoute une consommation mensuelle.
   * 
   * @param consommationMensuelle La consommation mensuelle à ajouter.
   */
  public void ajouteConsommationMensuelle(ConsommationMensuelle consommationMensuelle) {
    if (consommationMensuelle == null) {
      throw new IllegalArgumentException("L'argument consommationMensuelle doit ne pas être null");
    }
    this.consommationsMensuelles.add(consommationMensuelle);
  }

  /** Calcule et retourne la consommation du mois en kg de CO2. */
  public double calculerConsommationTotale() {
    return this.consommationsMensuelles.stream().mapToDouble(ut -> ut.calculerConsommation()).sum();
  }
}
