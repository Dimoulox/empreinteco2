package com.swing.model;

import com.swing.db.persistance.Persistable;

/**
 * Groupe contenant des éléments consommateurs.
 */
public class Categorie extends Persistable<Integer> {

  /** Taille minimale de la catégorie. */
  private static int TAILLE_MIN_NOM = 3;

  /** Taille maximale de la catégorie. */
  private static int TAILLE_MAX_NOM = 45;

  /** Nom de la catégorie. */
  private String nom;

  /**
   * Crée une catégorie avec le nom donné.
   * 
   * @param nom Le nom de la catégorie.
   */
  public Categorie(String nom) {
    if (nom == null) {
      throw new IllegalArgumentException("L'argument nom doit ne pas être null");
    }
    if (nom.length() < TAILLE_MIN_NOM) {
      throw new IllegalArgumentException(
          "L'argument nom doit être de plus de " + TAILLE_MIN_NOM + " caractères");
    }
    if (nom.length() >= TAILLE_MAX_NOM) {
      throw new IllegalArgumentException(
          "L'argument nom doit être de moins de " + TAILLE_MAX_NOM + " caractères");
    }
    this.nom = nom;
  }

  /** Retourne le nom de la catégorie. */
  public String getNom() {
    return nom;
  }
}
