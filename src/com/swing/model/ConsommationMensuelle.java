package com.swing.model;

import com.swing.db.persistance.Persistable;
import java.util.ArrayList;

/**
 * Consommation mensuelle d'un utilisateur.
 */
public class ConsommationMensuelle extends Persistable<Integer> {

  /** Liste d'utilisations d'éléments dans le mois. */
  private ArrayList<UtilisationElement> utilisationsElements;

  /** Année de la consommation. */
  private int annee;

  /** Mois de la consommation. */
  private Mois mois;

  /**
   * Crée une consommation mensuelle avec des utilisations d'éléments.
   * 
   * @param utilisationsElements La liste d'utilisations d'éléments dans le mois.
   * @param annee L'année de la consommation.
   * @param mois Le mois de la consommation.
   */
  public ConsommationMensuelle(ArrayList<UtilisationElement> utilisationsElements, int annee,
      Mois mois) {
    if (utilisationsElements == null) {
      throw new IllegalArgumentException("L'argument utilisationsElements doit ne pas être null");
    }
    if (mois == null) {
      throw new IllegalArgumentException("L'argument mois doit ne pas être null");
    }
    this.utilisationsElements = utilisationsElements;
    this.annee = annee;
    this.mois = mois;
  }

  /**
   * Crée une consommation mensuelle sans utilisations d'éléments.
   * 
   * @param annee L'année de la consommation.
   * @param mois Le mois de la consommation.
   */
  public ConsommationMensuelle(int annee, Mois mois) {
    if (mois == null) {
      throw new IllegalArgumentException("L'argument mois doit ne pas être null");
    }
    this.utilisationsElements = new ArrayList<UtilisationElement>();
    this.annee = annee;
    this.mois = mois;
  }

  /**
   * Ajoute une utilisation d'élément.
   * 
   * @param utilisationElement L'utilisation d'élément à ajouter.
   */
  public void ajouteUtilisationElement(UtilisationElement utilisationElement) {
    if (utilisationElement == null) {
      throw new IllegalArgumentException("L'argument utilisationElement doit ne pas être null");
    }
    this.utilisationsElements.add(utilisationElement);
  }

  /**
   * Calcule et retourne la consommation du mois en kg de CO2.
   */
  public double calculerConsommation() {
    return this.utilisationsElements.stream().mapToDouble(ut -> ut.calculerConsommation()).sum();
  }

  /** Retourne l'année de la consommation. */
  public int getAnnee() {
    return annee;
  }

  /** Retourne le mois de la consommation. */
  public Mois getMois() {
    return mois;
  }
}
