package com.swing.model;

import com.swing.db.persistance.Persistable;

/**
 * Utilisation d'un élément sur un mois.
 */
public class UtilisationElement extends Persistable<Integer> {

  /** Elément consommateur lié. */
  private ElementConsommateur elementConsommateur;

  /** Utilisation de l'élément consommateur. */
  private double utilisation;

  /**
   * Crée une utilisation d'élément à partir d'un élément consommateur et d'une utilisation.
   * 
   * @param elementConsommateur L'élément consommateur lié à l'utilisation.
   * @param utilisation L'utilisation de l'élément consommateur.
   */
  public UtilisationElement(ElementConsommateur elementConsommateur, double utilisation) {
    if (elementConsommateur == null) {
      throw new IllegalArgumentException("L'argument elementConsommateur doit ne pas être null");
    }
    if (utilisation < 0.) {
      throw new IllegalArgumentException("L'argument utilisation doit être supérieur ou égal à 0");
    }
    this.elementConsommateur = elementConsommateur;
    this.utilisation = utilisation;
  }

  /**
   * Calcule et retourne la consommation de l'élément en kg de CO2.
   */
  public double calculerConsommation() {
    return elementConsommateur.calculerConsommation(utilisation);
  }
}
