package com.swing.model;

import com.swing.db.persistance.Persistable;

/**
 * Utilisateur de l'application. Est hérité par les gestionnaires, analystes et particuliers.
 * 
 * @see Gestionnaire
 * @see Analyste
 * @see Particulier
 *
 */
public abstract class Profil extends Persistable<String> {

  /** Taille minimale du pseudo. */
  private static int TAILLE_MIN_PSEUDO = 3;

  /** Taille maximale du pseudo. */
  private static int TAILLE_MAX_PSEUDO = 15;

  /** Taille minimale du mot de passe. */
  private static int TAILLE_MIN_MOT_DE_PASSE = 3;

  /** Taille maximale du mot de passe. */
  private static int TAILLE_MAX_MOT_DE_PASSE = 15;

  /** Pseudo de l'utilisateur. */
  private String pseudo;

  /** Mot de passe de l'utilisateur. */
  private String motDePasse;

  /**
   * Crée un profil avec les paramètres donnés.
   * 
   * @param pseudo Le pseudo de l'utilisateur.
   * @param motDePasse Le mot de passe de l'utilisateur.
   */
  public Profil(String pseudo, String motDePasse) {
    if (pseudo == null) {
      throw new IllegalArgumentException("Le pseudo doit ne pas être null");
    }
    if (motDePasse == null) {
      throw new IllegalArgumentException("Le mot de passe doit ne pas être null");
    }
    if (pseudo.length() < TAILLE_MIN_PSEUDO) {
      throw new IllegalArgumentException(
          "Le pseudo doit être de plus de " + TAILLE_MIN_PSEUDO + " caractères");
    }
    if (pseudo.length() > TAILLE_MAX_PSEUDO) {
      throw new IllegalArgumentException(
          "Le pseudo doit être de moins de " + TAILLE_MAX_PSEUDO + " caractères");
    }
    if (motDePasse.length() < TAILLE_MIN_MOT_DE_PASSE) {
      throw new IllegalArgumentException(
          "Le mot de passe doit être de plus de " + TAILLE_MIN_MOT_DE_PASSE + " caractères");
    }
    if (motDePasse.length() > TAILLE_MAX_MOT_DE_PASSE) {
      throw new IllegalArgumentException(
          "Le mot de passe doit être de moins de " + TAILLE_MAX_MOT_DE_PASSE + " caractères");
    }
    this.pseudo = pseudo;
    this.motDePasse = motDePasse;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((pseudo == null) ? 0 : pseudo.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Profil other = (Profil) obj;
    if (pseudo == null) {
      if (other.pseudo != null) {
        return false;
      }
    } else if (!pseudo.equals(other.pseudo)) {
      return false;
    }
    return true;
  }

  /** Retourne le pseudo de l'utilisateur. */
  public String getPseudo() {
    return pseudo;
  }

  /** Retourne le mot de passe de l'utilisateur. */
  public String getMotDePasse() {
    return motDePasse;
  }
}
