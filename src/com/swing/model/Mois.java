package com.swing.model;

/**
 * Enumération des mois de l'année.
 */
public enum Mois {
  JANVIER, FEVRIER, MARS, AVRIL, MAI, JUIN, JUILLET, AOUT, SEPTEMBRE, OCTOBRE, NOVEMBRE, DECEMBRE;
}
