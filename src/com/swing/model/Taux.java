package com.swing.model;

import com.swing.db.persistance.Persistable;

public class Taux extends Persistable<Integer> {

  /** Taille minimale du nom. */
  private static int TAILLE_MIN_NOM = 3;

  /** Taille maximale du nom. */
  private static int TAILLE_MAX_NOM = 45;

  /** Nom du taux. */
  private String nom;

  /** Valeur du taux. */
  private double valeur;

  /**
   * Crée un taux avec les paramètres donnés.
   * 
   * @param valeur La valeur du taux.
   * @param nom Le nom du taux.
   */
  public Taux(double valeur, String nom) {
    if (valeur < 0.) {
      throw new IllegalArgumentException("L'argument valeur doit être supérieur ou égal à 0");
    }
    if (nom == null) {
      throw new IllegalArgumentException("L'argument nom doit ne pas être null");
    }
    if (nom.length() < TAILLE_MIN_NOM) {
      throw new IllegalArgumentException(
          "L'argument nom doit être de plus de " + TAILLE_MIN_NOM + " caractères");
    }
    if (nom.length() >= TAILLE_MAX_NOM) {
      throw new IllegalArgumentException(
          "L'argument nom doit être de moins de " + TAILLE_MAX_NOM + " caractères");
    }
    this.valeur = valeur;
    this.nom = nom;
  }

  /** Retourne la valeur du taux. */
  public double getValeur() {
    return valeur;
  }

  /** Retourne le nom du taux. */
  public String getNom() {
    return nom;
  }
}
