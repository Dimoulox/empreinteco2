package com.swing.model;

/**
 * Utilisateur dont le but est d'analyser les données des particuliers.
 */
public class Analyste extends Profil {

  /**
   * Crée un constructeur avec les paramètres donnés.
   * 
   * @param pseudo Le pseudo de l'analyste.
   * @param motDePasse Le mot de passe de l'analyste.
   */
  public Analyste(String pseudo, String motDePasse) {
    super(pseudo, motDePasse);
  }
}
