package com.swing.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Communications avec la BDD dans le cadre de la persistance des données.
 */
public class MariaDbQuest {

  /** Session de connexion avec la BDD. */
  private static Connection connexion;

  static {
    try {
      // Initialisation de la connexion
      connexion = DriverManager.getConnection(MariaDbConfig.dbUrl, MariaDbConfig.dbUtilisateur,
          MariaDbConfig.dbMotDePasse);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /** Retourne l'instance unique de connexion à la BDD. */
  public static Connection getConnexion() {
    return connexion;
  }

  /**
   * Exécute une requête SQL sur la BDD.
   * 
   * @param requete La requête à exécuter.
   * @return Le ResultSet de la requête donnée.
   * @throws SQLException En cas d'erreur d'exécution de la requête donnée.
   */
  public static ResultSet executerRequete(String requete) throws SQLException {
    return getConnexion().createStatement().executeQuery(requete);
  }

  /**
   * Permet de protéger un paramètre afin d'éviter les problèmes d'injections.
   * 
   * @param param Le paramètre à protéger.
   * @return Le paramètre protégé.
   */
  public static String proteger(Object param) {
    return param.toString().replace("\"", "\\\"").replace("\'", "\\\'");
  }

  /**
   * Exécute une requête protégée, avec un "?" pour chaque paramètre à insérer.
   * 
   * @param requete La requête à exécuter.
   * @param params Les paramètres à donner pour la requête.
   * @return Le ResultSet de la requête donnée.
   * @throws SQLException En cas d'erreur d'exécution de la requête donnée.
   */
  public static ResultSet executerRequeteProtegee(String requete, Object... params)
      throws SQLException {

    for (Object param : params) {
      if (!requete.contains("?")) {
        throw new IllegalArgumentException("Le nombre de paramètres donnée ne correspond pas au "
            + "nombre de \"?\" donnés dans la requête");
      }
      requete = requete.replaceFirst("(?:\\?)+", proteger(param.toString()));
    }
    if (requete.contains("?")) {
      throw new IllegalArgumentException("Le nombre de paramètres donnée ne correspond pas au "
          + "nombre de \"?\" donnés dans la requête");
    }
    return executerRequete(requete);
  }
}
