package com.swing.db.persistance;

import com.swing.db.MariaDbQuest;
import com.swing.model.Categorie;
import com.swing.model.ElementConsommateur;
import com.swing.model.Taux;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Permet de lire et d'écrire de nouveaux éléments consommateurs dans la BDD.
 */
public class PersistanceElementConsommateur {

  /**
   * Lit l'élément consommateur avec l'identifiant donné dans la BDD.
   * 
   * @param id Identifiant de l'élément à lire.
   * @return L'élément avec l'identifiant donné.
   */
  public static ElementConsommateur lireElementConsommateur(int id) {

    try {
      ResultSet rs =
          MariaDbQuest.executerRequeteProtegee("select * from equipement where equ_id = ?", id);
      if (rs.next()) {
        String nom = rs.getString("equ_nom");
        double consommation = rs.getDouble("equ_consommationUnitaire");
        // int partage = rs.getInt("equ_partage");
        int partage = 1;
        int categorieId = rs.getInt("cat_id");
        int tauxId = rs.getInt("tau_id");

        Categorie categorie = PersistanceCategorie.lireCategorie(categorieId);
        Taux taux = PersistanceTaux.lireTaux(tauxId);

        ElementConsommateur elementConsommateur =
            new ElementConsommateur(nom, categorie, taux, consommation, partage);
        elementConsommateur.setId(id);
        return elementConsommateur;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
}
