package com.swing.db.persistance;

/**
 * Désigne un objet persistable. Cette classe contient un identifiant permettant de faire le lien
 * avec l'objet persisté s'il existe.<br>
 * Si l'objet n'existe pas dans la BDD, l'identifiant est null.
 *
 * @param <T> Type de la clé primaire dans la BDD
 */
public abstract class Persistable<T> {

  /**
   * Identifiant du Persistable.
   */
  private T id;

  /**
   * Crée un Persistable avec un identifiant null.
   */
  public Persistable() {
    this.setId(null);
  }

  /**
   * Crée un Persistable.
   * 
   * @param id Identifiant à donner.
   */
  public Persistable(T id) {
    this.setId(id);
  }

  /**
   * Retourne l'identifiant du Persistable.
   * 
   * @return L'identifiant du Persistable.
   */
  public T getId() {
    return id;
  }

  /**
   * Modifie l'identifiant du Persistable.
   * 
   * @param id Nouvel identifiant.
   */
  public void setId(T id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    @SuppressWarnings("rawtypes")
    Persistable other = (Persistable) obj;
    if (id == null) {
      if (other.id != null) {
        return false;
      }
    } else if (!id.equals(other.id)) {
      return false;
    }
    return true;
  }
}
