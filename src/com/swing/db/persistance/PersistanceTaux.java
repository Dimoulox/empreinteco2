package com.swing.db.persistance;

import com.swing.db.MariaDbQuest;
import com.swing.model.Taux;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Permet de lire et d'écrire de nouveaux taux dans la BDD.
 */
public class PersistanceTaux extends MariaDbQuest {

  /**
   * Lit le taux avec l'identifiant donné dans la BDD.
   * 
   * @param id Identifiant du taux à lire.
   * @return Le taux avec l'identifiant donné.
   */
  public static Taux lireTaux(int id) {

    try {
      ResultSet rs =
          MariaDbQuest.executerRequeteProtegee("select * from Taux where tau_id = ?", id);
      if (rs.next()) {
        String nom = rs.getString("tau_nomTaux");
        double valeur = rs.getDouble("tau_valeur");

        Taux taux = new Taux(valeur, nom);
        taux.setId(id);
        return taux;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
}
