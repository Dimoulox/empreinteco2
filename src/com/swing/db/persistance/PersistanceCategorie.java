package com.swing.db.persistance;

import com.swing.db.MariaDbQuest;
import com.swing.model.Categorie;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Permet de lire et d'écrire de nouvelles catégories dans la BDD.
 */
public class PersistanceCategorie {

  /**
   * Lit la catégorie avec l'identifiant donné dans la BDD.
   * 
   * @param id Identifiant de la catégorie à lire.
   * @return La catégorie avec l'identifiant donné.
   */
  public static Categorie lireCategorie(int id) {

    try {
      ResultSet rs =
          MariaDbQuest.executerRequeteProtegee("select * from Categorie where cat_id = ?", id);
      if (rs.next()) {
        String nom = rs.getString("cat_nom");
        Categorie categorie = new Categorie(nom);
        categorie.setId(id);
        return categorie;
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    return null;
  }
}
