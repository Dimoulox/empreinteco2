package com.swing;

import com.swing.db.persistance.PersistanceElementConsommateur;
import com.swing.model.ElementConsommateur;

/** Classe d'exécution principale. */
public class MainClass {

  /**
   * Fonction {@code main} principale, actuellement utilisée pour les tests.
   * 
   * @param args Arguments d'exécution potentiels. Actuellement ignorés.
   */
  public static void main(String[] args) {

    // bien évidemment il faudra créer l'élément consommateur 1 dans la BDD au préalable
    ElementConsommateur ec = PersistanceElementConsommateur.lireElementConsommateur(1);
    System.out.println(ec.getNom());
  }

}
